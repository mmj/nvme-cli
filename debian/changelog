nvme-cli (1.12-6) experimental; urgency=medium

  [ Vasudeva Kamath ]
  * Move to team maintenance using tracker.d.o

  [ Madhu Adav M J ]
  * Update Maintainers and VCS fields in control file
  * Move systemd services to /lib/systemd
  * Remove dependencies on syslog.target
  * Remove source/include-binaries (Closes: #944833)
  * Add nvme-cli-udeb package
  * Update debian/copyright file
  * Remove files created by postinst script, on package purge
  * Build package with libsystemd
  * Add autopkgtest to check binary execution

 -- Madhu Adav M J <madhuadav@outlook.com>  Fri, 09 Apr 2021 19:45:51 +0530

nvme-cli (1.12-5) unstable; urgency=medium

  * Add uuid-runtime as dependency. (Closes: #970637)

 -- Breno Leitao <leitao@debian.org>  Tue, 22 Sep 2020 20:37:51 +0100

nvme-cli (1.12-4) unstable; urgency=medium

  * Fix udev rules and config files. Thanks intrigeri (Closes: #968769)
  * Remove debian/package.bash-completion (Closes: #969345)

 -- Breno Leitao <leitao@debian.org>  Sun, 20 Sep 2020 10:39:01 +0100

nvme-cli (1.12-3) unstable; urgency=medium

  * Bring debhelper compat to 13 and fix build deps (Closes: #969183)

 -- Breno Leitao <leitao@debian.org>  Sun, 20 Sep 2020 10:23:14 +0100

nvme-cli (1.12-2) unstable; urgency=medium

  *  Generate /etc/nvme/host* files at install time, not build time.
     Thanks Dan for the patch. (Closes: #969340)

 -- Breno Leitao <leitao@debian.org>  Sun, 20 Sep 2020 10:14:36 +0100

nvme-cli (1.12-1) unstable; urgency=medium

  * New upstream release.

 -- Breno Leitao <leitao@debian.org>  Sat, 20 Jun 2020 09:27:22 +0100

nvme-cli (1.9-1) unstable; urgency=medium

  * New upstream release
  * Fixed dependencies (Closes: #944832)

 -- Breno Leitao <leitao@debian.org>  Sat, 23 Nov 2019 13:40:40 +0000

nvme-cli (1.7-1) unstable; urgency=medium

  * New upstream release

 -- Breno Leitao <leitao@debian.org>  Mon, 21 Jan 2019 06:25:35 -0500

nvme-cli (1.6-1) unstable; urgency=medium

  * New upstream release

 -- Breno Leitao <leitao@debian.org>  Mon, 06 Aug 2018 12:12:32 -0400

nvme-cli (1.5-1) unstable; urgency=medium

  * New upstream release

 -- Breno Leitao <leitao@debian.org>  Mon, 29 Jan 2018 12:59:02 -0500

nvme-cli (1.4-1) unstable; urgency=medium

  * New upstream release
  * Standard Version upgraded to 4.0.1

 -- Breno Leitao <leitao@debian.org>  Mon, 02 Oct 2017 08:27:23 -0400

nvme-cli (1.3-1) unstable; urgency=medium

  * New upstream release. Thanks Rodrigo R. Galvão <rosattig@br.ibm.com>

 -- Breno Leitao <leitao@debian.org>  Wed, 14 Jun 2017 16:36:20 -0300

nvme-cli (1.1-1) unstable; urgency=medium

  * New upstream release

 -- Breno Leitao <leitao@debian.org>  Mon, 06 Feb 2017 08:20:45 -0500

nvme-cli (1.0-3) unstable; urgency=medium

  * Last commit didn't fix all the problem for 32-bits archs.
    Fixing it properly now. (Closes: #847232)

 -- Breno Leitao <leitao@debian.org>  Wed, 14 Dec 2016 07:51:29 -0500

nvme-cli (1.0-2) unstable; urgency=medium

  * Fix FTBFS in 32-bits arch. (Closes: #847232)

 -- Breno Leitao <leitao@debian.org>  Tue, 13 Dec 2016 08:31:42 -0500

nvme-cli (1.0-1) unstable; urgency=medium

  * New upstream release.

 -- Breno Leitao <leitao@debian.org>  Thu, 24 Nov 2016 15:15:28 -0500

nvme-cli (0.9-1) unstable; urgency=medium

  * New upstream release

 -- Breno Leitao <leitao@debian.org>  Thu, 06 Oct 2016 13:03:59 -0400

nvme-cli (0.8-2) unstable; urgency=medium

  * Add a patch to enable nvme-cli to compile on 32 bits system (Thanks Steve
    Langasek) (Closes: #830521)

 -- Breno Leitao <brenohl@br.ibm.com>  Mon, 11 Jul 2016 12:48:11 -0400

nvme-cli (0.8-1) unstable; urgency=medium

  * New upstream release

 -- Breno Leitao <brenohl@br.ibm.com>  Sun, 03 Jul 2016 07:12:26 -0400

nvme-cli (0.7-1) unstable; urgency=medium

  * New upstream release
  * Bump Standard-Version to 3.9.8, no changes required.

 -- Breno Leitao <brenohl@br.ibm.com>  Tue, 21 Jun 2016 16:32:22 -0400

nvme-cli (0.5-1) unstable; urgency=medium

  * New upstream release
    - Fix version number (Closes: #816438)
  * Bump Standard-Version to 3.9.7, no changes required.
  * drop 0004-Fix-English-typos-only.patch: upstream.

 -- Breno Leitao <brenohl@br.ibm.com>  Tue, 22 Mar 2016 10:43:12 -0400

nvme-cli (0.3-1) unstable; urgency=medium

  * Initial release. (Closes: #810033)

 -- Breno Leitao <brenohl@br.ibm.com>  Tue, 05 Jan 2016 14:40:05 -0500
